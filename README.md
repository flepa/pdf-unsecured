# PDF-Unsecured

This project makes an automation in removing password protection from protected PDF files in a given folder.

**Note**: this is only a personal application made for fun, please have mercy.

## Usage

`python pdf_unsecured.py <folder that contains pdf files>`
