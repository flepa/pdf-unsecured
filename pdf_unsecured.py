from pikepdf import Pdf, PasswordError
import os, pathlib, sys


def check_protected_pdf(pdf_path):
    '''
        This function checks if the given file path is related to a 
        protected pdf.
    '''
    try:
        pdf = Pdf.open(pdf_path)
    except PasswordError:
        return True
    else:
        return False


def save_unprotected_pdfs(paths):
    '''
        This function opens the files related to the given path list and 
        save these pdfs as unprotected (they will be overwritten).
    '''
    for path in paths:
        with Pdf.open(path, password=pdf_password,
                      allow_overwriting_input=True) as pdf:
            pdf.save(path)


if __name__ == '__main__':
    pdf_password = 'your-password'
    folder = pathlib.Path(sys.argv[1])  # folder to analyze

    pdf_files = [folder / file
                 for file in os.listdir(folder)
                 if file.endswith('.pdf') and
                 check_protected_pdf(folder / file)]
    save_unprotected_pdfs(pdf_files)
